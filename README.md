# PandasBassicTurortial

Algunos scripts para usar el package pandas de python (3.5) en Windows.
Indicaciones antes de clonar proyecto:

1. Instalar virtualenv:
`pip install virtualenv / python -m pip install virtualenv`

2. Comandos para crear virutalenv en el directorio del proyecto

```
mkdir ejemploPandas
cd ejemploPandas
git clone https://gitlab.com/flor54/pandasbassicturortial.git .
git remote remove origin
python -m virtualenv .
Scripts\activate
cd "Curso Pandas"
pip install -r requirements.txt
```


**Si se quiere leer otro archivo distinto de JULIO2019 debes crear tus propias funciones para leer ese archivo****
Para leer otro archivo csv se debe ejecutar los ejemplos01,02,03.... con un parametro adicional en consola. ejemplo:
` python ejemplo03.py miarchivo.csv`