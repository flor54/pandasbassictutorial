import pandas as pd
import sqlite3
import utils
import sys
import matplotlib.pyplot as plt
import seaborn as sns
import warnings
warnings.filterwarnings('ignore')

conn = sqlite3.connect('db.sqlite3')
cur = conn.cursor()
#cur.execute("select * from post_post limit 5;")
#results = cur.fetchall()
#print(results)

# se peude escribir una query con join, where, group y seleccion de campos 
'''
ejemplo:
select id, 
'''
df = pd.read_sql_query('SELECT au.username as UserName, pp.id as IdPost FROM post_post as pp INNER join auth_user as au on au.id  = pp.user_id ;', conn)

plt.hist(df.UserName,bins=15)
plt.show()
cur.close()
conn.close()

