import sys
import utils
''' diferencia entre iat y at.acceder a elementos individuales. es decir, una celda del dataframe'''

def operacion05(df): # resibe dataframe
	#cambiar nombre:
	utils.cambiarNombreColum(df)
	# .at ---> para etiquetas. los nros enteros tambine son etiquetas
	# .iat ---> para la locacion en nro entero
	print(df.iat[3,0])
	print(df.iat[0,1])
	# usar at
	print(df.at[2,'ClaveIncidencia'])
	# diferentes resultados entre at y iat 
	sub3 = df[::3] # sub conjunto
	print('imporimir un subconjunto del dattaframe;')
	print(sub3)
	print(sub3.iat[3,0]) # va x index
	print(sub3.at[3,'ClaveIncidencia']) # va por etiqueta que tiene el index

def operacion05MiFile(df):
	''' completar funcion '''
	pass
	
if __name__ == "__main__":
	try:
		file = sys.argv[1] # el argumento que de debe pasarse es un archivo csv
		df = utils.getDataFrameFromCSV(file)
		
		''' debes crear funciones para  tu archivo '''
		operacion05MiFile(df)
		
	except IndexError:
		df = utils.getDataFrameFromCSV("")
		operacion05(df)
	except:
		print(sys.exc_info()[0])