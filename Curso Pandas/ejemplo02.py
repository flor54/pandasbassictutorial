import utils
import sys
''' scripts con package pandas que sirven para visualizar datos de columna y transformarlas '''


def operacion02(df): # resive un dataframe
	# cambiar nombre de columnas
	utils.cambiarNombreColum(df)
	# sub filas
	df[1:6]
	df[10:]
	df[:10]
	# ver filas desde la ultima hasta la primera
	print(df[::-1])
	# ver filas de 10 en 10
	print(df[::10])
	# seleccionar columnas
	print(df["ClaveIncidencia"])
	df["ClaveIncidencia"].head()
	# dos columnas con head
	df[["ClaveIncidencia","DescripcionInsidencia"]].head() # --> no importa el orden de la columnas
	# rapido acceso a una columna
	df.ClaveIncidencia.head()
	# combinando para conseguir sub datos
	df[1:4][["ClaveIncidencia","DescripcionInsidencia"]].head()
	df[["ClaveIncidencia","DescripcionInsidencia"]][1:4].head() # --> no importa el orden

	
def operacion02MiFile(df):
	pass
	
	
## principal del programa	
if __name__ == "__main__":
	try:
		file = sys.argv[1] # el argumento que de debe pasarse es un archivo csv
		df = utils.getDataFrameFromCSV(file)
		''' debes crear funciones para  tu archivo '''
		operacion02MiFile(df)

	except IndexError:
		df = utils.getDataFrameFromCSV("")
		operacion02(df)
	except:
		print(sys.exc_info()[0])


	
