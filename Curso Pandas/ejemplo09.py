import utils
import sys

import matplotlib.pyplot as plt
#%matplotlib inline
import seaborn as sns
import warnings
warnings.filterwarnings('ignore')
sns.set_style('dark')
''' subplot. con subplot tenemos dos graficos en una misma ventana'''


def operacion08(df): # resive un dataframe
	''' Varios graficos... '''
	#cambio nombre columnas
	nombreColumnas = ['Pelicula','Genero','ClasificacionCriticas','Audiencia','Presupuesto','Año']
	utils.cambiarNombreColum(df,nombreColumnas)
	df.Año = df.Año.astype('category')
	df.Pelicula = df.Pelicula.astype('category')
	df.Genero = df.Genero.astype('category')
	
	f,ax = plt.subplots(1,2,figsize =(12,6)) # una fila dos columnas
	
	kl = sns.kdeplot(df.Presupuesto,df.Audiencia, ax = ax[0])
	k2 = sns.kdeplot(df.Presupuesto,df.ClasificacionCriticas, ax = ax[1])
	plt.show()
	
	# se debe especificar de otra forma si se tienen mas de dos chart
	f,ax = plt.subplots(2,2,figsize =(12,6),sharex=True,sharey=True) # dos filas dos columnas
	k1 = sns.kdeplot(df.Presupuesto,df.Audiencia, ax = ax[0,0])
	k2 = sns.kdeplot(df.Presupuesto,df.ClasificacionCriticas, ax = ax[1,0])
	k1.set(xlim=(-20,160))
	plt.show()
	
	
	
	
def operacion08MiFile(df):
	pass
	
	
## principal del programa	
if __name__ == "__main__":
	try:
		file = sys.argv[1] # el argumento que de debe pasarse es un archivo csv
		df = utils.getDataFrameFromCSV(file)
		''' debes crear funciones para  tu archivo '''
		operacion08MiFile(df)

	except IndexError:
		df = utils.getDataFrameFromCSV('Movie-Ratings.csv')
		operacion08(df)
	except:
		print(sys.exc_info()[0])


	
