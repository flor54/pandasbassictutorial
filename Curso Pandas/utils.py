import os
import sys
from pandas import read_csv

def getDataFrameFromCSV(file):
	# Conseguir path actual
	path = os.getcwd() + '\\'
	fileInput = checkFile(file)
	filename = path + fileInput
	# Abrir archivo CSV en formato dataframe ... 
	# aca ya cuento con los datos
	df = read_csv(filename)
	return df
	

def checkFile(file):
	if len(file) == 0 or file == '':
		return 'JULIO2019.csv'
	return file
	
def cambiarNombreColum(df,listaNombres=None):
	''' si no pasan parametro de cambio de columnas y el file es JULIO20190 entonces asigno mis propios nombres para el archivo que se usa en este ejemplo'''
	if(listaNombres == None):
		df.columns = ['ClaveIncidencia','DescripcionInsidencia','Horas','Usuario','FechaTrabajo','ClaveProyecto','NonbreProyecto','DescripcionTarea']
	else:
		df.columns = listaNombres

def leerColumnas(file):
	'''Conseguir nombre de columnas de mi archivo CSV (Primer Columna)'''
	f = open(filename,'r')
	colums = f.readline().split(',')
	return colums