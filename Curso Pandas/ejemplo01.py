import utils
import sys

def operacion01(df):
	''' operaciones basicas para visualizar estado gral. del dataframe '''
	# Saber cantidad de columnas que tengo : 
	print(len(df))
	# ver columnas
	print(df.columns)
	# nro de columnas
	print(len(df.columns))
	# primeras 5 filas : print(df.head())   ..  o solo las 2 primeras filas:
	print(df.head())
	# ultimas filas: print(df.tail()) o 2 ultimas filas:
	print(df.tail())
	# informacion de las columnas
	print(df.info())
	# funciones en las  columnas numericas:
	print(df.describe())
	# funciones en las  columnas numericas con otro formato:
	print(df.describe().transpose())

if __name__ == '__main__':
	try:
		file = sys.argv[1] # el argumento que de debe pasarse es un archivo csv
		df = utils.getDataFrameFromCSV(file)
		operacion01(df)
	except IndexError:
		df = utils.getDataFrameFromCSV('')
		operacion01(df)
	except:
		print(sys.exc_info()[0])
