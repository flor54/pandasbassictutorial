import utils
import sys

import matplotlib.pyplot as plt
#%matplotlib inline
import seaborn as sns
import warnings
warnings.filterwarnings('ignore')

''' joinplot. nuevo grafico. mas histogramas '''


def operacion08(df): # resive un dataframe
	''' Varios graficos... '''
	#cambio nombre columnas
	nombreColumnas = ['Pelicula','Genero','ClasificacionCriticas','ClasificacionAudiencia','Presupuesto','Año']
	utils.cambiarNombreColum(df,nombreColumnas)
	df.Año = df.Año.astype('category')
	df.Pelicula = df.Pelicula.astype('category')
	df.Genero = df.Genero.astype('category')
	# hacer grafina de distrubucion junto con histograma
	j = sns.jointplot(data=df, x='ClasificacionCriticas',y='ClasificacionAudiencia',kind='hex') # para personalizar grafico. datos en forma de hexagonal para grupos de datos
	plt.show()
	# histograma con seaborn
	m1 = sns.distplot(df.ClasificacionAudiencia,bins=15)
	plt.show()
	# histograma con pyplot
	m1 = plt.hist(df.ClasificacionAudiencia,bins=15)
	plt.show()
	# hsitograma por genero en plt que muestra el presupuesto x genero.
	pl1 = plt.hist(df[df.Genero == 'Action'].Presupuesto)
	pl1 = plt.hist(df[df.Genero == 'Drama'].Presupuesto)
	pl1 = plt.hist(df[df.Genero == 'Thriller'].Presupuesto)
	plt.show()
	# otra forma de hacer un histograma con dos generos 
	generos = [df[df.Genero == 'Action'].Presupuesto,df[df.Genero == 'Drama'].Presupuesto] # se puede hacer asi o es mejor hacer ciclos
	pl1 = plt.hist(generos,bins=15,stacked = True)
	plt.show()
	generos = []
	etiquetas = []
	for gen in df.Genero.cat.categories:
		generos.append(df[df.Genero == gen].Presupuesto)
		etiquetas.append(gen)
	plt.hist(generos,bins=15,label=etiquetas)
	plt.legend()
	plt.show()
	visl = sns.lmplot(data=df,x='ClasificacionCriticas',y='ClasificacionAudiencia',
		fit_reg=False,hue='Genero',size=7,aspect=1)
	plt.show()
	# muestra densidad de la grafica de distrubucion
	kl = sns.kdeplot(df.ClasificacionCriticas,df.ClasificacionAudiencia,
		shade=True, shade_lowest=False,cmap='Reds')
	plt.show()
	
	
	
	
def operacion08MiFile(df):
	pass
	
	
## principal del programa	
if __name__ == "__main__":
	try:
		file = sys.argv[1] # el argumento que de debe pasarse es un archivo csv
		df = utils.getDataFrameFromCSV(file)
		''' debes crear funciones para  tu archivo '''
		operacion08MiFile(df)

	except IndexError:
		df = utils.getDataFrameFromCSV('Movie-Ratings.csv')
		operacion08(df)
	except:
		print(sys.exc_info()[0])


	
