import sys
import utils
''' operaciones sobre columnas '''

# operaciones en dataframe
def operacion03(df): # resibe dataframe
	#cambiar nombre:
	utils.cambiarNombreColum(df)
	# suma:
	df.Horas + df.Horas
	# multiplicacion:
	df.Horas * df.Horas
	# agregar columna con un resultado:
	df['columnaCalculada'] = df.Horas * df.Horas
	print(df.head()) # se puede observar que la ultima columna es la que se acaba de crear..

	# eliminar columna:
	df = df.drop('columnaCalculada',1) # drop retorna un nuevo objeto.
	print(df.head()) # este nuevo objeto es el dataframe sin la columna que se habia agregado.

def operacion03MiFile(df):
	''' completar funcion '''
	pass
	
if __name__ == "__main__":
	try:
		file = sys.argv[1] # el argumento que de debe pasarse es un archivo csv
		df = utils.getDataFrameFromCSV(file)
		
		''' debes crear funciones para  tu archivo '''
		operacion03MiFile(df)
		
	except IndexError:
		df = utils.getDataFrameFromCSV("")
		operacion03(df)
	except:
		print(sys.exc_info()[0])
