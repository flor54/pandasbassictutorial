import sys
import utils
''' filtros en filas '''

# operaciones en dataframe
def operacion04(df): # resibe dataframe
	#cambiar nombre:
	utils.cambiarNombreColum(df)
	# cambiar tipo de dato de una columna
	df['Horas'].astype('int64')
	
	# filtar por hrs menor que 7
	filter1 = df.Horas < 7 # ---> devuelve True o False indicando que columna cumple la condicion con True.
	# se puede usar el filtro para saber que registros son de la siguiente manera:
	print(df[filter1]) # imprime que columnas cumplen con la condicion
	
	# combinar filtros
	filter2 = df.Usuario == 'FCORDOBA'
	# df[filter1 and filter2] --> no funciona!
	print(df[filter1 & filter2][['Usuario','Horas']]) # --> and 
	print(df[filter1 | filter2][['Usuario','Horas']])# --> or
	
	# unique de una columna
	print(df.Usuario.unique())
	
def operacion04MiFile(df):
	''' completar funcion '''
	pass
	
if __name__ == "__main__":
	try:
		file = sys.argv[1] # el argumento que de debe pasarse es un archivo csv
		df = utils.getDataFrameFromCSV(file)
		
		''' debes crear funciones para  tu archivo '''
		operacion04MiFile(df)
		
	except IndexError:
		df = utils.getDataFrameFromCSV("")
		operacion04(df)
	except:
		print(sys.exc_info()[0])

