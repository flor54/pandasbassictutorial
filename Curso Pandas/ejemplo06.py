import sys
import utils
import matplotlib.pyplot as plt # por convencion pyplot es plt
#%matplotlib inline
import seaborn as sns
import warnings
warnings.filterwarnings('ignore')
plt.rcParams['figure.figsize'] = 8,4

#tmatplotlib inline
''' visualizacion de datos '''

def operacion06(df): # resibe dataframe
	# histograma:
	vis1 = sns.distplot(df['NroItem']) # con medidas por debajo de 1...
	# grafica en bloques:
	vis2 = sns.boxplot(data=df,x='Correo',y='Cantidad')
	# grafico de dispersion. muestra una recta creciente o decreciente:
	vis3 = sns.lmplot(data=df,x='NroItem',y='Cantidad',fit_reg=False, # con le parametro fit_reg desaparece la linea
		hue='Correo',
		size=5, aspect=1,
		scatter_kws= {'s':200}) # diccionario que se usan para la llamada a la funcion matplotlib.pyplot.plot en plt. 
	plt.show()
	

def operacion65MiFile(df):
	''' completar funcion '''
	pass
	
if __name__ == "__main__":
	try:
		file = sys.argv[1] # el argumento que de debe pasarse es un archivo csv
		df = utils.getDataFrameFromCSV(file)
		''' debes crear funciones para  tu archivo '''
		operacion06MiFile(df)
		
	except IndexError:
		df = utils.getDataFrameFromCSV('VENTAS_EXAMPLE.csv')
		operacion06(df)
	except:
		print(sys.exc_info()[0])