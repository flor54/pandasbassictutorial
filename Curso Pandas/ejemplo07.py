import utils
import sys

''' categorias en tipos de datos '''


def operacion07(df): # resive un dataframe
	nombreColumnas = ['Pelicula','Genero','ClasificacionCriticas','ClasificacionAudiencia','Presupuesto','Año']
	utils.cambiarNombreColum(df,nombreColumnas)
	print('Columna Año es int64. hay que darle un tipo de dato correcto')
	print(df.info())
	df.Año = df.Año.astype('category') # hay que sobreescribir la columna!
	# Tambien hay que cambiarle el tipo de dato de Pelicula y Genero a 'category'
	df.Pelicula = df.Pelicula.astype('category')
	df.Genero = df.Genero.astype('category')
	
	print('con categorias podemos conseguir valores unicos ya que podemos clasificarlos clasificamos:')
	print(df.Genero.cat.categories)
	
def operacion07MiFile(df):
	pass
	
	
## principal del programa	
if __name__ == "__main__":
	try:
		file = sys.argv[1] # el argumento que de debe pasarse es un archivo csv
		df = utils.getDataFrameFromCSV(file)
		''' debes crear funciones para  tu archivo '''
		operacion07MiFile(df)

	except IndexError:
		df = utils.getDataFrameFromCSV('Movie-Ratings.csv')
		operacion07(df)
	except:
		print(sys.exc_info()[0])


	
